#!/bin/bash
set -e

# Task:
#   Render SVG files from ./path/file.svg to OUTDIR/path/file.tga
#   Overwrites existing tga files.
#
# Usage:
#   ./render.sh OUTDIR SVG...
#
# Examples:
#   One file:  ./render.sh ~/.xonotic/data gfx/crosshair64.svg
#   All files: ./render.sh ~/.xonotic/data $(find . -name "*.svg")
#
# Dependencies:
#   Inkscape (1.0.1)
#   ImageMagick (7.0.7)

outDir="$1"; shift

for svg in "$@"; do
	# Extract name components
	svgDir=$(dirname -- "$svg")
	svgName=$(basename -- "$svg" .svg)

	# Compose target file names
	dir="$outDir/$svgDir"
	tga="$dir/$svgName.tga"
	tmp="$dir/$svgName.tmp.png"

	# Ensure target directory exists
	mkdir --parent "$dir"

	# Render image
	inkscape --export-filename "$tmp" "$svg"
	convert -auto-orient "$tmp" -compress RLE "$tga"
	rm "$tmp"
done
