;Adds a white border and a faint glow, based on the alpha channel
;Input: filesIn (wildcard * possible) borderGrow borderFeather objectShrink objectFeather glowBlur
;Output: RLE compressed .tga file(s) (overwrites existing tga files)

(define (whiteborder filesIn glowBlur borderGrow borderFeather objectShrink objectFeather)
	(let*
        (
            (fileList (cadr (file-glob filesIn 1)))
            (borderColor '(255 255 255))
            (borderGlow '(223 223 223))
        )

        (while (not (null? fileList))
	        (let*
		        (
                    (file (car fileList))
			        (image (car (gimp-file-load 1 file file)))
			        (bottomLayer (car (gimp-image-get-active-layer image)))
			        (middleLayer (car (gimp-layer-new-from-drawable bottomLayer image)))
			        (topLayer (car (gimp-layer-new-from-drawable bottomLayer image)))
		        )

                ;Add layers
		        (gimp-image-add-layer image middleLayer -1)
		        (gimp-image-add-layer image topLayer -1)

                ;Bottom layer: create extended glow siluette of the object
                (gimp-context-set-foreground borderGlow)
                (gimp-selection-layer-alpha bottomLayer)
                (gimp-selection-grow image (- borderGrow 1))
                (gimp-selection-feather image borderFeather)
                (gimp-edit-fill bottomLayer 0)
                (gimp-selection-none image)
                (plug-in-gauss-rle 1 image bottomLayer glowBlur 1 1)

                ;Middle layer: create extended siluette of the object
                (gimp-context-set-foreground borderColor)
                (gimp-selection-layer-alpha middleLayer)
                (gimp-selection-grow image borderGrow)
                (gimp-selection-feather image borderFeather)
                (gimp-edit-fill middleLayer 0)

                ;Top layer: reduce border of the object on the top layer
                (gimp-selection-layer-alpha topLayer)
                (gimp-selection-shrink image objectShrink)
                (gimp-selection-feather image objectFeather)
                (gimp-selection-invert image)
                (gimp-edit-clear topLayer)

                ;Set extension to .tga
		        (set! file (car (strbreakup file ".")))
		        (set! file (string-append file ".tga")) 

                ;Save image as RLE compressed tga
		        (set! bottomLayer (car (gimp-image-merge-visible-layers image 1)))
		        (file-tga-save 1 image bottomLayer file file 1 0)
                (gimp-image-delete image)

                (set! fileList (cdr fileList))
            )
	    )
    )
)
