IMPORTANT NOTE:
ok_hmg sources were exported using `modeldecompile` in-game command without changing their extensions. Furthermore, the exported SMD sources from the game were imported to Blender and heavily modified to be exact the same as the original compiled ones.

g_ok_hmg and v_ok_hmg are IQM format files, if you wanna use them, export those ones to IQM and rename the extension to MD3.

h_ok_hmg is a DPM format file, if you wanna use it, export the mesh and animations to SMD (you need Blender Source Tools add-on installed in Blender). After use dpmodel from https://icculus.org/twilight/darkplaces/files/dpmodel20091008beta1.zip 
write and modify the .txt contains for dpmodel (like box.txt sample). 
Be careful when you modify, it could give bad results, put the origins correctly, in case of doubt, look the other ok weapon .txt examples. 
After export execing the command in CMD/Terminal: 
`dpmodel.exe your_weapon_name.txt` (Windows) 
`dpmodel your_weapon_name.txt` (Linux)

Once done that, rename the extensions of h_*.dpm and h_*.dpm.framegroups to h_*.iqm and h_*.iqm.framegroups
