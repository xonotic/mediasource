#!/bin/sh

infile=$1
outfile=$2
end=$3
guess=$4
guess2=$5
fftsize=${6:-32768}

gcc -o findloop findloop.c -lfftw3 -Wall -Wextra -ffast-math -lsndfile -lm -O3
./findloop "$infile" "$fftsize" "$end" "$guess" "$guess2" temp.wav | while read -r SAMPLES SECONDS; do
	oggenc -q 10 -o "$outfile" -c LOOP_START=$SAMPLES temp.wav
	rm -f temp.wav
	echo "To repeat, seek to $SAMPLES"
	vorbisgain "$outfile"
done
