#!/bin/sh

n=`vorbiscomment "$1" | grep LOOP_START= | cut -d = -f 2`

sox "$1" temp.wav trim "$n"s
play --combine concatenate "$1" temp.wav temp.wav temp.wav temp.wav temp.wav temp.wav temp.wav
rm -f temp.wav
