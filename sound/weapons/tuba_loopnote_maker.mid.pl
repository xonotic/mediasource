#!/usr/bin/perl

use strict;
use warnings;
use MIDI;
use MIDI::Opus;

my ($file, $delta, $program, $velocity, $transpose, @notes) = @ARGV;

my $track = MIDI::Track->new();

$track->new_event('set_tempo', 0, $delta * 1000000);
$track->new_event('patch_change', 0, 0, $program - 1);
my $first = 1;
for(@notes)
{
	$track->new_event('note_on', $first ? 0 : 96, 0, $transpose + $_, $velocity);
	$track->new_event('note_off', 96, 0, $transpose + $_, $velocity);
	$first = 0;
}

my $opus = MIDI::Opus->new({
		format => 0,
		ticks => 96,
		tracks => [ $track ],
		});

$opus->write_to_file($file);
