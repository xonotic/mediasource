#!/bin/sh

set -e

(
	cd loopfinder
	gcc -o findloop findloop.c -lfftw3 -Wall -Wextra -ffast-math -lsndfile -lm -O3
)

t=`mktemp -dt loopfinder.XXXXXX`
trap 'rm -rf "$t"' EXIT

synth=$1; shift
program=$1; shift
velocity=$1; shift
transpose=$1; shift
tubaid=$1; shift

delta=2
fftsize=2048
loopmin=0.5
loopmax=1.5
len=1.8

perl tuba_loopnote_maker.mid.pl "$t"/tuba_loopnote_maker.mid $delta $program $velocity $transpose -18 -12 -6 0 6 12 18 24
cp "$t"/tuba_loopnote_maker.mid tuba$tubaid.mid

case "$synth" in
	timidity)
		timidity -Ow "$@" -o "$t"/out.wav "$t"/tuba_loopnote_maker.mid
		;;
	fluidsynth)
		fluidsynth -v -n -i -l -T wav -F "$t"/out.wav "$@" "$t"/tuba_loopnote_maker.mid
		;;
	lmms)
		echo "Import $t/tuba_loopnote_maker.mid into LMMS, set instrument, and export as $t/out.wav"
		lmms
		[ -f "$t"/out.wav ]
		;;
esac

normalize --peak "$t"/out.wav

start=0
step=$(($delta*2))
for note in -18 -12 -6 0 6 12 18 24; do
	sox "$t"/out.wav "$t"/n$note.wav \
		channels 1 \
		trim $start $step \
		silence 1 1s 0

	fn=tuba"$tubaid"_loopnote"$note".ogg 

	# now find loop point
	loopfinder/findloop "$t"/n$note.wav $fftsize $len $loopmin $loopmax "$t"/t$note.wav | while read -r SAMPLES SECONDS; do
		oggenc -Q -q9 -o "$fn" -c "LOOP_START=$SAMPLES" "$t"/t$note.wav
	done

	# next!
	start=$(($start+$step))
done

exit 0
