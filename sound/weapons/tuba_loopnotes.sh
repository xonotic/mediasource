#!/bin/sh

# tuba = 59
sh tuba_loopnote_maker.sh timidity 59 100 48 ""  -EFreverb=G4

# accordeon = 24
sh tuba_loopnote_maker.sh timidity 24 100 60 "1" -EFreverb=G4

# klein bottle = LMMS SID sound
# LMMS settings: 
sh tuba_loopnote_maker.sh lmms 0 100 60 "2"
